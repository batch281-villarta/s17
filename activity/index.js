// console.log("Hello world!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

function printUserDetails() {
  let fullname = prompt("Enter your full name: ");
  let age = prompt("Enter your age: ");
  let location = prompt("Enter your location: ");

  console.log(`Hello, ${fullname}.`);
  console.log(`You are ${age} years old.`);
  console.log(`You live in ${location}.`);
}

//first function here:
printUserDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function printFavoriteBands() {
  const favoriteBands = [
    "The Beatles",
    "Metallica",
    "The Eagles",
    "Eraserheads",
    "Parokya Ni Edgar",
  ];

  favoriteBands.forEach((band, i) => {
    console.log(`${i + 1}. ${band}`);
  });
}

//second function here:
printFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function printFavoriteMovies() {
  const favoriteMovies = {
    "Breaking bad": 96,
    "The 100": 93,
    "Prison Break": 61,
    "The Godfather": 97,
    "The Psycho": 96,
  };

  Object.entries(favoriteMovies).forEach(([title, rating], i) => {
    console.log(`${i + 1}. ${title}`);
    console.log(`Rotten Tomatoes Rating: ${rating}%`);
  });
}

//third function here:
printFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printUsers = function () {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  console.log("You are friends with:");
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
};

printUsers();
